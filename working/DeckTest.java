/**
 * 
 */
package com.lnguyen.game.CardGame;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * @author ext.Long.Nguyen
 *
 */
class DeckTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterEach
	void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.lnguyen.game.CardGame.Deck#Deck()}.
	 */
	@Test
	void testDeck() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.lnguyen.game.CardGame.Deck#Deck(com.lnguyen.game.CardGame.Deck)}.
	 */
	@Test
	void testDeckDeck() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.lnguyen.game.CardGame.Deck#Deck(int)}.
	 */
	@Test
	void testDeckInt() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.lnguyen.game.CardGame.Deck#size()}.
	 */
	@Test
	void testSize() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.lnguyen.game.CardGame.Deck#sort()}.
	 */
	@Test
	void testSort() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.lnguyen.game.CardGame.Deck#shuffle()}.
	 */
	@Test
	void testShuffle() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.lnguyen.game.CardGame.Deck#swap(com.lnguyen.game.CardGame.Card, com.lnguyen.game.CardGame.Card)}.
	 */
	@Test
	void testSwap() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.lnguyen.game.CardGame.Deck#dealOneCard()}.
	 */
	@Test
	void testDealOneCard() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.lnguyen.game.CardGame.Deck#show()}.
	 */
	@Test
	void testShow() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.lnguyen.game.CardGame.Deck#showDiscarded()}.
	 */
	@Test
	void testShowDiscarded() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.lnguyen.game.CardGame.Deck#reset()}.
	 */
	@Test
	void testReset() {
		fail("Not yet implemented");
	}

}
