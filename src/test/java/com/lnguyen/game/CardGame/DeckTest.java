package com.lnguyen.game.CardGame;

import junit.framework.TestCase;

public class DeckTest extends TestCase {

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testSize() {
		Deck testDeck = new Deck();
		
		assertEquals("-----> Not matched", 52, testDeck.size());
	}
	
	public void testSort() {
		int i = 0;
		Deck testDeck = new Deck();
		testDeck.sort();

		for(Rank r: Rank.values()) {
			for(Suit s: Suit.values()) {
				if( i > testDeck.size() - 1)
					fail("-----> Sort fails 1");
				assertEquals("-----> Sort fails 2", r, testDeck.getDeck().get(i).getRank());
				assertEquals("-----> Sort fails 3", s, testDeck.getDeck().get(i).getSuit());
				i++;
			}
		}		
	}

}
