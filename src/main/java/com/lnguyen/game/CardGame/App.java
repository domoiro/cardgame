package com.lnguyen.game.CardGame;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 * Welcome to LNGUYEN's Card game!
 *
 */
public class App 
{
    public static void main( String[] args ) throws IOException
    {
        System.out.println( "------------ Welcome to Card game!!! -----------" );

        Scanner stdin = new Scanner(System.in);
        System.out.print("\nHow many decks of card you want to play? (0 to quit): ");

        boolean isOut = true; 
        while (true) {
            String s = stdin.next();
            try {
                int n = Integer.parseInt(s);
                if (n > 0) {
    	        	Deck playDeck = new Deck(n);
    	            while (isOut) {
    	            	System.out.println("\nDeck is ready. What do you want to do next?");
    	            	System.out.print("1. Show deck\n2. Shuffle\n3. Sort\n4. Validate\n5. Deal card\n6. Show discarded cards\n7. Reset deck\n0. Back to main menu\n\nEnter your choice: ");
    	                s = stdin.next();
    	                try {
    	                	n = Integer.parseInt(s);
	    	                switch(n) {
	    	                	case 1:
	    	                		playDeck.show();
	    	                		break;
	    	                	case 2:
	    	                		playDeck.shuffle();
	    	                		break;
	    	                	case 3:
	    	                		playDeck.sort();
	    	                		break;
	    	                	case 4:
	    	                        Deck cloneDeck = new Deck(playDeck);
	    	                        cloneDeck.sort();
	    	                        cloneDeck.show();
	    	                        cloneDeck = null;
	    	                        break;
	    	                	case 5:
	    	        	            while (n > 0) {
		    	                		System.out.print("Enter number of cards you want to deal (0 to go back): ");
	    	        	                s = stdin.next();
	    	        	                try {
	    	        	                	n = Integer.parseInt(s);
	    	        	                    if (n > 0) {
	    	        	                        int j = 0;
	    	        	                        while(playDeck.size() > 0 && j < n) {
	    	        	                	        System.out.println(++j + ". Deal one card: "+playDeck.dealOneCard().show());
	    	        	                        }
	    	        	                        System.out.println("\n");
	    	        	                    }
	    	        	                    else {
	    	        	                    	throw new NumberFormatException ();
	    	        	                    }

	    	        	                } catch (NumberFormatException e) {
	    	        	                	System.out.print("You are kidding, right? Please enter a positive number\n");
	    	        	                }
	    	        	                
	    	        	            }
	    	        	            
	    	                		break;
	    	                	case 6:
	    	                		playDeck.showDiscarded();
	    	                		break;
	    	                	case 7:
	    	                		playDeck.reset();
	    	                		break;
	    	                	case 0:
	    	                		isOut = false;
	    	                		break;
	    	                	default:
	    	                		System.out.println("\nInvalid choice. Please select option from 0-7");
	    	                		break;
	    	                		
	    	                }

    	                } catch (NumberFormatException e) {
    	                	System.out.print("You are kidding, right? Please enter a positive number\n");
    	                }
    	                
    	            }
    	            playDeck = null;
    	        	isOut = true;
    	        }
    	        else if(n == 0) {
                    break;
                }
    	        else if(n < 0)
    	        	throw new NumberFormatException();
                
            } catch (NumberFormatException e) {
                System.out.print("You are kidding, right? Please enter a positive number\n");
            }
            System.out.print("\nHow many decks of card you want to play? (0 to quit): ");
        }
        	
    	System.out.println("\n------ Thank you for playing. Have a nice day!!! -------\n");
    	stdin.close();

    }
}
