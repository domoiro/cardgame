package com.lnguyen.game.CardGame;

import java.util.Collections;
import java.util.LinkedList;
import java.util.Random;

public class Deck implements DeckWork<Card>{
	private LinkedList <Card> deck = new LinkedList<Card>();
	private LinkedList <Card> discarded = new LinkedList<Card>();
	
	Deck(){
		for(Suit suit: Suit.values()) {
			for(Rank rank: Rank.values()) {
				Card c = new Card(rank,suit);
				deck.push(c);
				discarded.clear();
			}
		}
	}

	Deck(Deck d){
		deck = (LinkedList<Card>) d.deck.clone();
		discarded = (LinkedList<Card>) d.discarded.clone();
	}
	
	Deck(int numOfDecks){
		Deck temp = new Deck();
		while(numOfDecks > 0) {
			temp = new Deck();
			deck.addAll(temp.deck);
			numOfDecks--;
		}
	}
	
	public LinkedList<Card> getDeck(){
		return deck;
	}
		
	public int size() {
		return deck.size();
	}
	
	public void sort(){
		Collections.sort(deck);
	}
	
	public void shuffle() {
		System.out.println("\n-------- shuffling deck ...");

		int deckSize = deck.size();
		int halfDeckSize = deckSize/2;
		int i = deckSize;
		Random r = new Random();
		
		while(--i >= halfDeckSize) {
			int seed = (int) r.nextInt(halfDeckSize + 1);
			swap(deck.get(i),deck.get(seed));
			
//			If allowed, use Collections.swap() otherwise use customized swap()
//			Collections.swap(deck, i, seed);
		}
	}

	public void swap(Card c1, Card c2) {
		Card temp = new Card(c1);
		c1.change(c2);
		c2.change(temp);
	}
	
	public Card dealOneCard() {
		if(size() > 0) {
			discarded.add(deck.pop());
			return discarded.getLast();
		}
		return new Card();
	}
	
	public void show() {
		System.out.println("\nNum of Cards: " + size());
		int i = 0;
		for(Card c: deck) {
			System.out.println(String.format("%2s",++i) + ". " + c.show() + " ");
		}
	}
	
	public void showDiscarded() {
		System.out.println("\nNum of Discarded Cards: " + discarded.size());
		int i = 0;
		for(Card c: discarded) {
			System.out.println(String.format("%2s",++i) + ". " + c.show() + " ");
		}
	}
	
	public void reset() {
		deck.addAll(0, discarded);
		discarded = new LinkedList<Card>();
	}
}
