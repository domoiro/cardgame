package com.lnguyen.game.CardGame;

public enum Suit implements Comparable<Suit> {
	HEART,
	CLUB,
	DIAMOND,
	SPADE
}
