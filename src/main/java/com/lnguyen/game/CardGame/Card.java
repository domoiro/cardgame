package com.lnguyen.game.CardGame;

public class Card implements CardWork<Card> {

	private Rank rank;
	private Suit suit;
	
	Card(){
		rank = null;
		suit = null;
	}
	
	Card(Rank r, Suit s){
		rank = r;
		suit = s;
	}
	
	Card(Card c){
		rank = c.rank;
		suit = c.suit;
	}
	
	public Card change(Rank r, Suit s) {
		rank = r;
		suit = s;
		return this;
	}
	
	public Card change(Card c) {
		if(this.compareTo(c)!=0) {
			this.change(c.rank, c.suit);
		}
		return this;
	}
	
	public Rank getRank() {
		return rank;
	}
	
	public Suit getSuit() {
		return suit;
	}
	
	public String show() {
		if(this.rank == null || this.suit == null)
			return "";
		
		return String.format("%-5s",rank.toString()) + " of " + suit.toString();
	}
	
	@Override
	public int compareTo(Card c) {
		// TODO Auto-generated method stub
		if(this.rank == c.rank && this.suit == c.suit )
			return 0;
		else if(this.rank.compareTo(c.rank) < 0 || this.rank.compareTo(c.rank) == 0 && this.suit.compareTo(c.suit) < 0)
			return -1;
		else return 1;
	}

}
