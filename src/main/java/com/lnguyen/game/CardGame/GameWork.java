package com.lnguyen.game.CardGame;

/**
 * @author LNGUYEN
 *
 */
public interface GameWork {
	public void sort();
	public void show();	
	public void reset();	
}
