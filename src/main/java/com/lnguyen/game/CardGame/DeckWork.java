package com.lnguyen.game.CardGame;

public interface DeckWork<E> extends GameWork {
	
	public void shuffle();
	public void swap(E e1, E e2);
}
