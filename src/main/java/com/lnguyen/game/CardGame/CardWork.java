package com.lnguyen.game.CardGame;

/**
 * @author LNGUYEN
 *
 */
public interface CardWork<T> extends Comparable<T>{

	public T change(T t);
	public String show();
	@Override
	public int compareTo(T t);

}